var sendResponse = require('./sendResponse');
var fs = require('node-fs');
var AWS = require('aws-sdk');
var generatePassword = require('password-generator');

exports.checkBlank = function (arr) {

    return (checkBlank(arr));
};

function checkBlank(arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] === '' || arr[i] === undefined || arr[i] === '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

exports.checkBlankWithCallback = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
};
exports.checkEmailAvailability = function (res, email, status,callback) {
if (status==1)
    { var sql = "SELECT `user_id` FROM `tb_users` WHERE `email`=? limit 1";}
    else
    { var sql = "SELECT `driver_id` FROM `tb_drivers` WHERE `email`=? limit 1";}
        connection.query(sql, [email], function (err, response) {

        if (err) {

            sendResponse.somethingWentWrongError(res);
        }
        else if (response.length) {

            sendResponse.emailExists(res);
        }
        else {
            callback();
        }
    });
}

exports.checkEmailRegistration = function (res, email,status, callback) {
    if (status==1)
    { var sql = "SELECT `user_id` FROM `tb_users` WHERE `email`=? limit 1";}
    else
    { var sql = "SELECT `driver_id` FROM `tb_drivers` WHERE `email`=? limit 1";}
    connection.query(sql, [email], function (err, response) {

        if (err) {

            sendResponse.somethingWentWrongError(res);
        }
        else if (response.length) {

            callback(null);
        }
        else {
            sendResponse.emailNotRegistered(res);

        }
    });
}
exports.generateRandomString = function () {
    var math = require('math');

    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < 4; i++)
        text += possible.charAt(math.floor(math.random() * possible.length));

    return text;
};
exports.uploadFile = function (file, folder, res, callback) {
    var filename = file.name;
    var path = file.path;
    var mimeType = file.type;
    fs.readFile(path, function (error, file_buffer) {
        if (error) {
            sendResponse.somethingWentWrongError(res);
        }

        filename = file.name;
        remotePath = config.get("production").Url + filename;
        filePath = config.get("production").Url;
        if (remotePath !== filePath) {
            AWS.config.update({
                accessKeyId: config.get('production').awsAccessKey,
                secretAccessKey: config.get('production').awsSecretKey
            });
            var s3bucket = new AWS.S3();
            var params = {
                Bucket: config.get('production').awsBucket,
                Key: folder + '/' + filename,
                Body: file_buffer,
                ACL: 'public-read',
                ContentType: 'mimetype'
            };

            s3bucket.putObject(params, function (err, data) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {

                    return callback(remotePath);

                }
            });
        }
        else {
            sendResponse.fileNotSelected(res);
        }
    });
};

exports.email_login_function = function (email, password, status, res) {
    if (status === 1) {
        var sql = "SELECT `login_status`FROM `tb_users` WHERE `email`=? LIMIT 1"
        connection.query(sql, [email], function (err, result) {
            if (err) {
                console.log(err);
                sendResponse.somethingWentWrongError(res);
            } else {
                if (result.length == 0) {
                    sendResponse.emailNotRegistered(res);
                }
                else if (result[0].login_status == 1) {
                    sendResponse.alreadyLoggedIn(res);
                }
                else {
                    var md5 = require('MD5');
                    var hash = md5(password);
                    var sql = "SELECT `access_token`,`phone`,`user_id`,`Fname`,`Lname`,`email`,`encryptPassword`,`date_registered`,`device_token`,`device_type`FROM `tb_users` WHERE `email`=? && `encryptPassword`=? LIMIT 1"
                    connection.query(sql, [email, hash], function (err, result_user) {
                        if (err) {

                            sendResponse.somethingWentWrongError(res);

                        }
                        else {
                            if (result_user.length == 0) {
                                sendResponse.passwordIncorrect(res);
                            }
                            else {
                                var final = {
                                    "Fname": result_user[0].Fname,
                                    "Lname": result_user[0].Lname,
                                    "email": result_user[0].email,
                                    "Password": result_user[0].encryptPassword,
                                    "phone": result_user[0].phone,
                                    "access_token": result_user[0].access_token,
                                    "date_registered": result_user[0].date_registered,
                                    "device_token": result_user[0].device_token,
                                    "device_type": result_user[0].device_type
                                };
                                sendResponse.successLogin(final, res);
                            }
                        }

                    });
                }
            }
        });
    }
    else {
        var sql = "SELECT `login_status`FROM `tb_drivers` WHERE `email`=? LIMIT 1"
        connection.query(sql, [email], function (err, result) {
            if (err) {
        console.log(err);
                sendResponse.somethingWentWrongError(res);
            } else {
                if (result.length == 0) {
                    sendResponse.emailNotRegistered(res);
                }
                else if (result[0].login_status == 1) {
                    sendResponse.alreadyLoggedIn(res);
                }
                else {
                    var md5 = require('MD5');
                    var hash = md5(password);
                    var sql = "SELECT `access_token`,`phone`,`driver_id`,`Fname`,`Lname`,`email`,`encryptPassword`,`date_registered`,`device_token`,`device_type`FROM `tb_drivers` WHERE `email`=? && `encryptPassword`=? LIMIT 1"
                    connection.query(sql, [email, hash], function (err, result_user) {
                        if (err) {

                            sendResponse.somethingWentWrongError(res);

                        }
                        else {
                            if (result_user.length == 0) {
                                sendResponse.passwordIncorrect(res);
                            }
                            else {
                                var final = {
                                    "Fname": result_user[0].Fname,
                                    "Lname": result_user[0].Lname,
                                    "email": result_user[0].email,
                                    "Password": result_user[0].encryptPassword,
                                    "phone": result_user[0].phone,
                                    "access_token": result_user[0].access_token,
                                    "date_registered": result_user[0].date_registered,
                                    "device_token": result_user[0].device_token,
                                    "device_type": result_user[0].device_type
                                };

                                sendResponse.successLogin(final, res);
                            }
                        }

                    });
                }
            }
        });


    }

};
//exports.driver_email_login_function = function (email, password,deviceToken,deviceType, res) {
//
//    var sql = "SELECT `login_status`,`verified` FROM `tb_users` WHERE `email`=? LIMIT 1";
//    connection.query(sql, [email], function (err, result) {
//
//        if (err) {
//            sendResponse.somethingWentWrongError(res);
//        }
//        else if (result[0].login_status === 1) {
//            sendResponse.alreadyLoggedIn(res);
//        }
//        else {
//            var md5 = require('MD5');
//            var hash = md5(password);
//            var sql = "SELECT `user_id` FROM `tb_users` WHERE `email`=? LIMIT 1";
//            connection.query(sql, [email], function (err, result) {
//                if (err) {
//                    sendResponse.somethingWentWrongError(res);
//
//                }
//                else {
//                    var length2 = result.length;
//                    if (length2 == 0) {
//
//                        sendResponse.emailNotRegistered(res);
//                    }
//                    else {
//                        var user = result[0].user_id;
//
//                        var sql = "SELECT `access_token`,`phone`,`user_id`,`Fname`,`Lname`,`email`,`encryptPassword`,`date_registered`,`device_token`,`device_type`\n\
//             FROM `tb_users` WHERE `user_id`=? && `encryptPassword`=? LIMIT 1"
//                        connection.query(sql, [user, hash], function (err, result_user) {
//                            if (err) {
//
//                                sendResponse.somethingWentWrongError(res);
//
//                            }
//                            else {
//                                console.log(result_user);
//                                var result_user_length = result_user.length;
//                                if (result_user_length === 0) {
//
//                                    sendResponse.passwordIncorrect(res);
//                                }
//                                else {
//
//                                    var final = {
//                                        "Fname": result_user[0].Fname,
//                                        "Lname": result_user[0].Lname,
//                                        "email": result_user[0].email,
//                                        "Password": result_user[0].encryptPassword,
//                                        "phone": result_user[0].phone,
//                                        "access_token": result_user[0].access_token,
//                                        "date_registered": result_user[0].date_registered,
//                                        "device_token":result_user[0].device_token,
//                                        "device_type":result_user[0].device_type
//                                    };
//                                    console.log(final);
//                                    sendResponse.successLogin(final, res);
//
//                                    //sendResponse.successLogin(result_user, res);
//                                }}
//                        });
//                    }
//                }
//            });
//        }
//
//    });
//
//
//}
    exports.sendMailToCustomer = function (userName, email) {
        var toEmail = email;
        var sub = "Welcome To Clicklabs";
        var html = '<html>';
        html += '<head>';
        html += '<title>congratulations</title>';
        html += '</head>';
        html += '<body>';


        html += '<p>You are successfully registered</p>';

        html += '</body>';
        html += '</html>';
        sendEmail(toEmail, html, sub, function (result) {
            if (result === 1) {
                console.log("Mail Sent");

            }
            else {
                console.log("Error Sending Mail");
            }
        });


    };

    exports.sendPasswordMailToCustomer = function (email,status, res) {
        var password = generatePassword(6, false);
        var md5 = require('MD5');
        var encrypted_pass = md5(password);
        var to = email;
        var sub = "Password Reset";
        var html = 'Information to reset your password. Copy the link below to reset your password. This link is valid for one use only.' + encrypted_pass;

        sendEmail(to, html, sub, function (result) {

            if (result == 1) {
if(status==1)
{ var sql3 = "UPDATE `tb_users` SET `one_time_link`=?,`password_reset`=? WHERE `email`=? LIMIT 1"}
                else
{ var sql3 = "UPDATE `tb_drivers` SET `one_time_link`=?,`password_reset`=? WHERE `email`=? LIMIT 1"}
                connection.query(sql3, [encrypted_pass, 0, to], function (err, result4) {

                    if (err) {
                        sendResponse.sendError(res);
                    }
                    else {
                        sendResponse.mailSent(res);
                    }

                });

            }


            else {

                sendResponse.sendError(res);
            }
        });

    }

    function sendEmail(receiverMailId, message, subject, callback) {
        var nodemailer = require("nodemailer");
        var smtpTransport = nodemailer.createTransport("SMTP", {
            service: "Gmail",
            auth: {
                user: config.get('emailSettings').email,
                pass: config.get('emailSettings').password

            }
        });

// setup e-mail data with unicode symbols
        var mailOptions = {
            from: config.get('emailSettings').email,
            to: receiverMailId,
            subject: subject,
            text: message

        };

// send mail with defined transport object
        smtpTransport.sendMail(mailOptions, function (error, info) {

            if (error) {
                return callback(0);
                console.log(error);
            } else {
                return callback(1);
                console.log('Message sent');
            }
        });
    }
    ;


    exports.change_password_function = function (userId, oldPassword, newPassword,status ,req, res) {
        if(status==1){
            var md5 = require('MD5');
        var sql = "SELECT `encryptPassword` FROM `tb_users` WHERE `user_id`=? LIMIT 1"
        connection.query(sql, [userId], function (err, result) {
            var password1 = md5(oldPassword);
            if (password1 == result[0].encryptPassword) {
                var password2 = md5(newPassword);

                var sql = "UPDATE `tb_users` SET `encryptPassword`=?  WHERE `user_id`=? LIMIT 1"
                connection.query(sql, [password2, userId], function (err, result5) {


                    sendResponse.sendPassSuccessData(res);

                });

            }
            else {

                sendResponse.sendError(res);
            }

        });

    }else{
            var md5 = require('MD5');
            var sql = "SELECT `encryptPassword` FROM `tb_drivers` WHERE `driver_id`=? LIMIT 1"
            connection.query(sql, [userId], function (err, result) {
                var password1 = md5(oldPassword);
                if (password1 == result[0].encryptPassword) {
                    var password2 = md5(newPassword)
                    var sql = "UPDATE `tb_drivers` SET `encryptPassword`=?  WHERE `driver_id`=? LIMIT 1"
                    connection.query(sql, [password2, userId], function (err, result5) {

                        sendResponse.sendPassSuccessData(res);

                    });

                }
                else {

                    sendResponse.sendError(res);
                }

            });
        }
}