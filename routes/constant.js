/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value:        value,
        enumerable:   true,
        writable:     false,
        configurable: false
    });
}

exports.responseStatus = {};

define(exports.responseStatus, "PARAMETER_MISSING", 100);
define(exports.responseStatus, "ERROR_IN_EXECUTION", 102);
define(exports.responseStatus, "SHOW_ERROR_MESSAGE", 103);
define(exports.responseStatus, "SHOW_MESSAGE", 104);
define(exports.responseStatus, "SHOW_DATA", 105);
define(exports.responseStatus, "EMAIL_NOT_REGISTERED", 106);
define(exports.responseStatus, "PASSWORD_INCORRECT", 107);
define(exports.responseStatus, "EMAIL_EXISTS", 108);
define(exports.responseStatus, "SHOW_LOGIN_DATA", 109);
define(exports.responseStatus, "SHOW_LOGGED_DATA", 110);
define(exports.responseStatus, "SHOW_LOGOUT_DATA", 111);
define(exports.responseStatus, "INVALID_ACCESS_TOKEN",113);
define(exports.responseStatus, "SHOW_ACCESS_LOGIN_DATA",112);
define(exports.responseStatus, "UPDATE_PROFILE",114);
define(exports.responseStatus, "MAIL_SENT", 115);
define(exports.responseStatus, "SHOW_LINK_ERROR_MESSAGE", 116);
define(exports.responseStatus, "CHANGE_PASSWORD",117);
define(exports.responseStatus, "DRIVER_NOT_VERIFIED",118);
define(exports.responseStatus, "NO_DRIVER_AVAILABLE",119);
define(exports.responseStatus, "SHOW_DRIVERS",120);
exports.responseMessage = {};
define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");
define(exports.responseMessage, "ERROR_IN_EXECUTION", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_ERROR_MESSAGE", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_MESSAGE", "Hi there!");
define(exports.responseMessage, "SHOW_DATA", "");
define(exports.responseMessage, "EMAIL_EXISTS", "this email is already registered");
define(exports.responseMessage, "SUCCESSFUL_EXECUTION", "Registration Successful.");
define(exports.responseMessage, "SUCCESSFUL_LOGIN", "LOGIN Successful.");
define(exports.responseMessage, "SUCCESSFUL_LOGOUT", "LOGOUT Successful.");
define(exports.responseMessage, "ALREADY_LOGGED_IN", "You're already logged in");
define(exports.responseMessage, "EMAIL_NOT_REGISTERED", "Oops your email has not been registered. Please sign up to login");
define(exports.responseMessage, "PASSWORD_INCORRECT", "Sorry, your password is incorrect");
define(exports.responseMessage, "INVALID_ACCESS_TOKEN","Invalid access token error");
define(exports.responseMessage, "MAIL_SENT", "Mail Sent Successfully.");
define(exports.responseMessage, "SHOW_LINK_ERROR_MESSAGE", "Sorry the link has expired");
define(exports.responseMessage, "CHANGE_PASSWORD","Password Changed Successfully.");
define(exports.responseMessage, "UPDATE_PROFILE","Profile updated successfully");
define(exports.responseMessage, "DRIVER_NOT_VERIFIED","Driver not verified");
define(exports.responseMessage, "NO_DRIVER_AVAILABLE","Driver not available");
define(exports.responseMessage, "SHOW_DRIVERS","Drivers available");