var func = require('./commonFunction');
var sendResponse = require('./sendResponse');
var express = require('express');
var router = express.Router();
var generatePassword = require('password-generator');
var moment = require('moment');
var md5 = require('MD5');
var math = require('math');
var request = require("request");
var http = require("http");
var async = require("async");
var geolib=require("geolib");
router.post('/login', function (req, res) {
    var user = req.body.user;
    var pass = req.body.pass;


    var check = [user, pass];
    var flag = func.checkBlank(check);
    if (flag) {
        sendResponse.parameterMissingError(res);
    }
    else {

        func.email_login_function(user, pass,1, res);

    }

});

router.post('/driverLogin', function (req, res) {
    var user = req.body.user;
    var pass = req.body.pass;


    var check = [user, pass];
    var flag = func.checkBlank(check);
    if (flag) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql="SELECT `verified` FROM `tb_drivers` WHERE `email`=? LIMIT 1"
        connection.query(sql,[user],function(err,response){
            if(err)
            {
                sendResponse.somethingWentWrongError(res);
            }
            else if(response[0].verified===0){
                sendResponse.notVerified(res);
            }
            else{
                func.email_login_function(user, pass, 0,res);
            }
        });
    }

});


router.post('/create', function (req, res) {
    var Fname = req.body.firstname;
    var Lname = req.body.lastname;
    var email = req.body.email;
    var password = req.body.password;
    var phone = req.body.phone;
    var deviceToken = req.body.device_token;
    var deviceType = req.body.device_type;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var checkBlank = [Lname, Fname, email, password, phone, deviceToken, deviceType,latitude,longitude];
    console.log(checkBlank);
    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, checkBlank, callback);

            },
            function (callback) {
                func.checkEmailAvailability(res, email,1, callback);
            }
        ],
        function (updatePopup) {
            func.uploadFile(req.files.user_image, 'apurva_files', res, function (result_file) {
                var md5 = require('MD5');
                var hash = md5(password);
                var accesstoken = func.generateRandomString();
                var accesstoken1 = accesstoken + email;
                var accessToken = md5(accesstoken1);
                var loginTime = new Date();

                var sql = "INSERT into tb_users(Fname,Lname,email,encryptPassword,phone,access_token,date_registered,last_login,device_token,device_type,latitude,longitude,file_path) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
                connection.query(sql, [Fname, Lname, email, hash, phone, accessToken, loginTime, loginTime, deviceToken, deviceType,latitude,longitude, result_file,], function (err, result) {

                    if (err) {

                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var sql = "SELECT Fname,Lname,email FROM tb_users WHERE email=? LIMIT 1";
                        connection.query(sql, [email], function (err, user, fields) {


                            var final = {
                                "Fname": user[0].Fname,
                                "Lname": user[0].Lname,
                                "email": user[0].email,
                                "Password": user[0].encryptPassword,
                                "phone": user[0].phone,
                                "access_token": user[0].access_token,
                                "date_registered": user[0].date_registered,
                                "device_token": user[0].device_token,
                                "device_type": user[0].device_type
                            };
                            sendResponse.sendSuccessData(final, res);
                            var userName = user[0].Fname;
                            func.sendMailToCustomer(userName, email);
                        });
                    }

                });


            });


        });
});

router.post('/driverCreate', function (req, res) {
    var Fname = req.body.firstname;
    var Lname = req.body.lastname;
    var email = req.body.email;
    var password = req.body.password;
    var phone = req.body.phone;
    var deviceToken = req.body.device_token;
    var deviceType = req.body.device_type;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var checkBlank = [Lname, Fname, email, password, phone, deviceToken, deviceType,latitude,longitude];
    console.log(checkBlank);
    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, checkBlank, callback);

            },
            function (callback) {
                func.checkEmailAvailability(res, email,0, callback);
            }
        ],
        function (updatePopup) {
            func.uploadFile(req.files.user_image, 'apurva_files', res, function (result_file) {
                var md5 = require('MD5');
                var hash = md5(password);
                var accesstoken = func.generateRandomString();
                var accesstoken1 = accesstoken + email;
                var accessToken = md5(accesstoken1);
                var loginTime = new Date();

                var sql = "INSERT into tb_drivers(Fname,Lname,email,encryptPassword,phone,access_token,date_registered,last_login,device_token,device_type,latitude,longitude,file_path,verified) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                connection.query(sql, [Fname, Lname, email, hash, phone, accessToken, loginTime, loginTime, deviceToken, deviceType,latitude,longitude, result_file,0], function (err, result) {

                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var sql = "SELECT Fname,Lname,email FROM tb_drivers WHERE email=? LIMIT 1";
                        connection.query(sql, [email], function (err, user, fields) {


                            var final = {
                                "Fname": user[0].Fname,
                                "Lname": user[0].Lname,
                                "email": user[0].email,
                                "Password": user[0].encryptPassword,
                                "phone": user[0].phone,
                                "access_token": user[0].access_token,
                                "date_registered": user[0].date_registered,
                                "device_token": user[0].device_token,
                                "device_type": user[0].device_type
                            };
                            sendResponse.sendSuccessData(final, res);
                            var userName = user[0].Fname;
                            func.sendMailToCustomer(userName, email);
                        });
                    }

                });


            });


        });
});


router.post('/accessTokenLogin', function (req, res) {

        var accessToken = req.body.access_token;

        var manvalues = [accessToken];
        var checkdata = func.checkBlank(manvalues);
        if (checkdata === 1) {
            sendResponse.parameterMissingError(res);
        }
        else {
            var sql = "SELECT `user_id`,`device_type`,`device_token`,`Fname`,`Lname`,`file_path`,`phone`,`email`,`encryptPassword`,`date_registered`,`device_type`,`device_token` FROM `tb_users` WHERE `access_token`=? LIMIT 1 "
            connection.query(sql, [accessToken], function (err, result) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else if (result.length !== 0) {
                    var md5 = require('MD5');
                    var accesstoken = func.generateRandomString();
                    var accesstoken1 = accesstoken + result[0].email;
                    var accessToken2 = md5(accesstoken1);
                    var sql = "UPDATE `tb_users` set `access_token`=?,`login_status`=? WHERE `user_id`=? LIMIT 1 "
                    connection.query(sql, [accessToken2,1, result[0].user_id], function (err, resultUpdate) {
                        if (err) {
                            sendResponse.somethingWentWrongError(res);
                        }

                        else {

                            var final = {
                                "user_name": result[0].Fname,
                                "user_image": result[0].user_image,
                                "access_token": result[0].access_token,
                                "email": result[0].email,
                                "phone_no": result[0].phone,
                                "device_token": result[0].device_token,
                                "device_type": result[0].device_type
                            }
                            sendResponse.sendAccessLoginSuccessData(final, res);
                        }


                    });
                }
            });
        }

});
router.post('/driverAccessTokenLogin', function (req, res) {

        var accessToken = req.body.access_token;
                var manvalues = [accessToken];
        var checkdata = func.checkBlank(manvalues);

        if (checkdata === 1) {
            sendResponse.parameterMissingError(res);

        }
        else {
            var sql = "SELECT `driver_id`,`Fname`,`Lname`,`file_path`,`phone`,`email`,`encryptPassword`,`date_registered`,`device_type`,`device_token` FROM `tb_drivers` WHERE `access_token`=? LIMIT 1 "
            connection.query(sql, [accessToken], function (err, result) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else if (result.length !== 0) {

                    var md5 = require('MD5');
                    var accesstoken = func.generateRandomString();
                    var accesstoken1 = accesstoken + result[0].email;
                    var accessToken2 = md5(accesstoken1);
                    var sql = "UPDATE `tb_drivers` set `access_token`=?,`login_status`=? WHERE `driver_id`=? LIMIT 1 "
                    connection.query(sql, [accessToken2,1, result[0].user_id], function (err, resultUpdate) {
                        if (err) {
                            sendResponse.somethingWentWrongError(res);
                        }

                        else {

                            var final = {
                                "user_name": result[0].Fname,
                                "user_image": result[0].user_image,
                                "access_token": result[0].access_token,
                                "email": result[0].email,
                                "phone_no": result[0].phone,
                                "device_token": result[0].device_token,
                                "device_type": result[0].device_type
                            }
                            sendResponse.sendAccessLoginSuccessData(final, res);
                        }


                    });
                }
            });
        }

});
router.post('/logout', function (req, res, next) {

    var accessToken = req.body.access_token;

    var sql = "SELECT `login_status` FROM `tb_users` WHERE `access_token`=? LIMIT 1";

    connection.query(sql, [accessToken], function (err, result) {
        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else if (result.length === 0) {
            sendResponse.invalidAccessTokenError(res);
        }
        else {

            var sql = "UPDATE `tb_users` SET `login_status`=? WHERE `access_token`=? LIMIT 1";
            connection.query(sql, [0, accessToken], function (err, result) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    sendResponse.sendSuccessLogout(res);
                }
            });
        }
    });
});
router.post('/driverLogout', function (req, res, next) {

    var accessToken = req.body.access_token;

    var sql = "SELECT `login_status` FROM `tb_drivers` WHERE `access_token`=? LIMIT 1";

    connection.query(sql, [accessToken], function (err, result) {
        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else if (result.length === 0) {
            sendResponse.invalidAccessTokenError(res);
        }
        else {

            var sql = "UPDATE `tb_drivers` SET `login_status`=? WHERE `access_token`=? LIMIT 1";
            connection.query(sql, [0, accessToken], function (err, result) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    sendResponse.sendSuccessLogout(res);
                }
            });
        }
    });
});
router.post('/editProfile', function (req, res) {

    var accessToken = req.body.access_token;
    var phone = req.body.phone;
    var email = req.body.email;
    var firstName = req.body.Fname;
    var lastName = req.body.Lname;

    var values = [accessToken, phone, firstName, lastName, email];
    var checkData = func.checkBlank(values);

    if (checkData == 1) {

        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `user_id`FROM `tb_users` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [accessToken], function (err, result) {
            if (err) {

                sendResponse.somethingWentWrongError(res);
            }
            else {
                if (result == 0) {
                    sendResponse.invalidAccessTokenError(res);
                }
                else {

                    file = req.files.user_image;
                    filename = file.name;
                    console.log(filename);
                    remotePath = config.get("production").Url + filename;
                    console.log(remotePath);
                    filePath = config.get("production").Url;
                    console.log(filePath);
                    if (remotePath === filePath) {

                        var sql1 = "UPDATE `tb_users` SET `Fname`=?,`Lname`=?,`phone`=?,`email`=? WHERE `user_id`=? LIMIT 1"
                        connection.query(sql1, [firstName, lastName, phone, email, result[0].user_id], function (err, result) {

                            if (err) {

                                sendResponse.somethingWentWrongError(res);
                            }
                            else {

                                sendResponse.updateProfile(res);
                            }


                        });
                    }


                    else {

                        func.uploadFile(req.files.user_image, 'apurva_files', res, function (result_file) {
                            console.log(result_file);
                            var sql2 = "UPDATE `tb_users` SET `file_path`=?,`Fname`=?,`Lname`=?,`phone`=?,`email`=?WHERE `user_id`=? LIMIT 1 ";
                            connection.query(sql2, [result_file, firstName, lastName, phone, email, result[0].user_id], function (err, result_update) {
                                if (err) {

                                    sendResponse.somethingWentWrongError(res);
                                }
                                else {
                                    sendResponse.updateProfile(res);
                                }
                            });

                        });
                    }
                }
            }
        });
    }});
router.post('/driverEditProfile', function (req, res) {

    var accessToken = req.body.access_token;
    var phone = req.body.phone;
    var email = req.body.email;
    var firstName = req.body.Fname;
    var lastName = req.body.Lname;

    var values = [accessToken, phone, firstName, lastName, email];
    var checkData = func.checkBlank(values);

    if (checkData == 1) {

        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `driver_id`FROM `tb_drivers` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [accessToken], function (err, result) {
            if (err) {

                sendResponse.somethingWentWrongError(res);
            }
            else {
                if (result == 0) {
                    sendResponse.invalidAccessTokenError(res);
                }
                else {

                    file = req.files.user_image;
                    filename = file.name;
                    console.log(filename);
                    remotePath = config.get("production").Url + filename;
                    console.log(remotePath);
                    filePath = config.get("production").Url;
                    console.log(filePath);
                    if (remotePath === filePath) {

                        var sql1 = "UPDATE `tb_drivers` SET `Fname`=?,`Lname`=?,`phone`=?,`email`=? WHERE `driver_id`=? LIMIT 1"
                        connection.query(sql1, [firstName, lastName, phone, email, result[0].driver_id], function (err, result) {

                            if (err) {

                                sendResponse.somethingWentWrongError(res);
                            }
                            else {

                                sendResponse.updateProfile(res);
                            }


                        });
                    }


                    else {

                        func.uploadFile(req.files.user_image, 'apurva_files', res, function (result_file) {

                            var sql2 = "UPDATE `tb_drivers` SET `file_path`=?,`Fname`=?,`Lname`=?,`phone`=?,`email`=?WHERE `driver_id`=? LIMIT 1 ";
                            connection.query(sql2, [result_file, firstName, lastName, phone, email, result[0].driver_id], function (err, result_update) {
                                if (err) {

                                    sendResponse.somethingWentWrongError(res);
                                }
                                else {
                                    sendResponse.updateProfile(res);
                                }
                            });

                        });
                    }
                }
            }
        });
    }});
router.post('/forgot', function (req, res) {
    var email = req.body.email;

    var manValues = [email];
    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);

        },
        function (callback) {
            func.checkEmailRegistration(res, email,1, callback);

        }], function (updatePopup) {

        var sql = "SELECT user_id,Fname FROM tb_users WHERE email=? LIMIT 1";
        connection.query(sql, [email], function (err, response12) {

            if (response12.length === 1) {
                func.sendPasswordMailToCustomer(email,1,res);

            }
            else {

                sendResponse.sendError(res);
            }

        });
    });
});
router.post('/driverForgot', function (req, res) {
    var email = req.body.email;

    var manValues = [email];
    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);

        },
        function (callback) {
            func.checkEmailRegistration(res, email, 0,callback);

        }], function (updatePopup) {

        var sql = "SELECT driver_id,Fname FROM tb_drivers WHERE email=? LIMIT 1";
        connection.query(sql, [email], function (err, response12) {

            if (response12.length === 1) {
                func.sendPasswordMailToCustomer(email,0,res);

            }
            else {

                sendResponse.sendError(res);
            }

        });
    });
});
router.post('/reset', function (req, res) {
    var link = req.body.link;
    var new_password = req.body.pass;
    var manValues = [link, new_password];

    var checkData = func.checkBlank(manValues);
    if (checkData === 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `user_id` FROM `tb_users` WHERE `one_time_link`=? LIMIT 1"
        connection.query(sql, [link], function (err, result5) {

            if (!err) {
                var md5 = require('MD5');
                var hash = md5(new_password);
                var sql = "SELECT `password_reset` FROM `tb_users` WHERE `one_time_link`=? LIMIT 1"
                connection.query(sql, [link], function (err, result57) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        if (result57[0].password_reset == 0) {


                            var sql2 = "UPDATE `tb_users` SET `one_time_link`=?,`encryptPassword`=?,`password_reset`=? WHERE `user_id`=? LIMIT 1"

                            connection.query(sql2, [link, hash, 1, result5[0].user_id], function (err, result56) {
                                if (err) {
                                    sendResponse.somethingWentWrongError(res);
                                }
                                else {

                                    sendResponse.sendPassSuccessData(res);
                                }

                            });
                        }
                        else {
                            sendResponse.linkExpire(res);
                        }
                    }

                });
            }
            else {
                sendResponse.somethingWentWrongError(res);
            }
        });


    }

});
router.post('/driverReset', function (req, res) {
    var link = req.body.link;
    var new_password = req.body.pass;
    var manValues = [link, new_password];

    var checkData = func.checkBlank(manValues);
    if (checkData === 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `driver_id` FROM `tb_drivers` WHERE `one_time_link`=? LIMIT 1"
        connection.query(sql, [link], function (err, result5) {

            if (!err) {
                var md5 = require('MD5');
                var hash = md5(new_password);
                var sql = "SELECT `password_reset` FROM `tb_drivers` WHERE `one_time_link`=? LIMIT 1"
                connection.query(sql, [link], function (err, result57) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        if (result57[0].password_reset == 0) {


                            var sql2 = "UPDATE `tb_drivers` SET `one_time_link`=?,`encryptPassword`=?,`password_reset`=? WHERE `driver_id`=? LIMIT 1"

                            connection.query(sql2, [link, hash, 1, result5[0].driver_id], function (err, result56) {
                                if (err) {
                                    sendResponse.somethingWentWrongError(res);
                                }
                                else {

                                    sendResponse.sendPassSuccessData(res);
                                }

                            });
                        }
                        else {
                            sendResponse.linkExpire(res);
                        }
                    }

                });
            }
            else {
                sendResponse.somethingWentWrongError(res);
            }
        });


    }

});
router.post('/changePassword', function (req, res) {
    console.log("hahaha");
    var accessToken = req.body.access_token;
    var oldPassword = req.body.old_password;
    var newPassword = req.body.new_password;
    var values = [accessToken, oldPassword, newPassword];
    var checkData = func.checkBlank(values);
    if (checkData == 1)
    {
        sendResponse.parameterMissingError(res);
    }
    else
    {
        var sql = "SELECT `user_id`FROM `tb_users` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [accessToken], function (err, result) {
            if (err) {

                sendResponse.somethingWentWrongError(res);
            }
            else {
                if (result == 0) {
                    sendResponse.invalidAccessTokenError(res);
                }
                else {
                    var userId = result[0].user_id;

                    func.change_password_function(userId, oldPassword, newPassword,1, req, res);
                }
            }
        });
    }
});
router.post('/driverChangePassword', function (req, res) {

    var accessToken = req.body.access_token;
    var oldPassword = req.body.old_password;
    var newPassword = req.body.new_password;
    var values = [accessToken, oldPassword, newPassword];
    var checkData = func.checkBlank(values);
    if (checkData == 1)
    {
        sendResponse.parameterMissingError(res);
    }
    else
    {
        var sql = "SELECT `driver_id`FROM `tb_drivers` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [accessToken], function (err, result) {
            if (err) {

                sendResponse.somethingWentWrongError(res);
            }
            else {
                if (result == 0) {
                    sendResponse.invalidAccessTokenError(res);
                }
                else {
                    var driverId = result[0].driver_id;

                    func.change_password_function(driverId, oldPassword, newPassword,0, req, res);
                }
            }
        });
    }
});
router.post('/showDriver', function (req, res) {
    var latitude1 = req.body.latitude;
    var longitude1 = req.body.longitude;

    var values = [latitude1,longitude1];

    var checkData = func.checkBlank(values);
    if (checkData == 1)
    {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `latitude`,`longitude` FROM `tb_drivers` WHERE `verified`=?"
        connection.query(sql, [1],function (err, result) {
            if (err) {

                sendResponse.somethingWentWrongError(res);
            }
            else {
                if(result.length===0)
                {
                    sendResponse.noDriverAvailable(res);
                }
                 else{

                    var drivers=[];
                    var len=result.length;
                    for(var i=0;i<len;i++)
                    {

                       var temp= geolib.isPointInCircle(
                            {latitude: latitude1, longitude: longitude1},
                            {latitude: result[i].latitude, longitude: result[i].longitude},
                            5000
                        );

                        if(temp)

                        {

                            drivers.push([result[i].latitude,result[i].longitude])
                             var sortedArray=geolib.orderByDistance({latitude: latitude1, longitude: longitude1},drivers);

                        }

                    }
                    if(drivers.length==0)
                    {sendResponse.noDriverAvailable(res);}
                    else{

                        sendResponse.showDrivers(sortedArray,res);

                    }
                }


            }
        });
    }
});
router.post('/searchDrivers', function (req, res) {
    var latitude1 = req.body.latitude;
    var longitude1 = req.body.longitude;

    var values = [latitude1,longitude1];

    var checkData = func.checkBlank(values);
    if (checkData == 1)
    {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `latitude`,`longitude` FROM `tb_drivers` WHERE `verified`=?"
        connection.query(sql, [1],function (err, result) {
            if (err) {

                sendResponse.somethingWentWrongError(res);
            }
            else {
                if(result.length===0)
                {
                    sendResponse.noDriverAvailable(res);
                }
                else{

                    var drivers=[];
                    var len=result.length;
                    for(var i=0;i<len;i++)
                    {

                        var temp= geolib.isPointInCircle(
                            {latitude: latitude1, longitude: longitude1},
                            {latitude: result[i].latitude, longitude: result[i].longitude},
                            5000
                        );

                        if(temp)

                        {

                            drivers.push([result[i].latitude,result[i].longitude])
                            var sortedArray=geolib.orderByDistance({latitude: latitude1, longitude: longitude1},drivers);

                        }

                    }
                    if(drivers.length==0)
                    {sendResponse.noDriverAvailable(res);}
                    else{

                        sendResponse.showDrivers(sortedArray,res);

                    }
                }


            }
        });
    }
});

module.exports = router;