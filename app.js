config = require('config');
var express = require("express");
var app = express();
var connection = require('./routes/mySqlLib');
var index = require('./routes/index');
var users = require('./routes/users');
var bodyParser = require('body-parser');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.set('view engine', 'jade');

app.get('/', index);


app.post('/create', multipartMiddleware);
app.post('/create', users);
app.post('/driverCreate', multipartMiddleware);
app.post('/driverCreate', users);
app.post('/login', users);
app.post('/driverLogin', users);
app.post('/accessTokenLogin', users);
app.post('/driverAccessTokenLogin', users);
app.post('/logout', users);
app.post('/driverLogout', users);
app.post('/editProfile', multipartMiddleware);
app.post('/editProfile', users);
app.post('/driverEditProfile', multipartMiddleware);
app.post('/driverEditProfile', users);

app.post('/forgot',users);
app.post('/driverForgot',users);
app.post('/reset',users);
app.post('/driverReset',users);
app.post('/changePassword',users);
app.post('/driverChangePassword',users);
app.post('/showDriver',users);
app.post('/searchDrivers',users);
app.listen(config.get('PORT'));
console.log("Express server started on port %d", config.get('PORT'));